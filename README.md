# Swagger Auto Mock to Heroku

This repository contains example implementation of generating swagger mock server, and host it to heroku.

Just run this command:

```sh
#!/bin/sh

rm -rf api controllers service utils package.json package-lock.json index.js 

# Change the swagger.json into your open api specification file
docker run --rm -v ${PWD}:/out -v ${PWD}/swagger.json:/opt/swagger.json swaggerapi/swagger-codegen-cli-v3 generate \
    -i /opt/swagger.json \
    -l nodejs-server \
    -o out

echo "INFO: Changing serverPort to use environment variable"
sed -i 's/var serverPort = 8080;/var serverPort = process.env.PORT || 8080;/' index.js

echo "INFO: Make the server run as public server on 0.0.0.0"
sed -i 's/http.createServer(app).listen(serverPort, function () {/http.createServer(app).listen(serverPort, "0.0.0.0", function () {/' index.js

echo "INFO: Import CORS middleware"
sed -i '5 i const cors = require("cors")' index.js

echo "INFO: Apply CORS middleware to the server"
sed -i '19 i app.use(cors())' index.js

sudo chown -R $(whoami) .

echo "INFO: Installing CORS middleware"
npm install cors
```

Or if you want to be more concise, you can run this instead:

```sh
bin/codegen
```
